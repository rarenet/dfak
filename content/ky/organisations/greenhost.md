---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 саат
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost IT кызматын сунуштап, анын негизги өзгөчөлүктөрү бул этика жана ишенимдүүлүк болуп саналат. Кызматтардын спектри веб-хостингди, булутта сактоо кызматтары, маалыматтык коопсуздук чөйрөсүндөгү функционалдык  чечимдерди камтыйт. Greenhost ачык булактуу программаларды түзүүгө жигердүү катышып, технология, журналистика, маданият, билим берүү, туруктуулук жана интернет эркиндиги жаатындагы ар кандай долбоорлорго өз салымдарын кошуп келет. 
