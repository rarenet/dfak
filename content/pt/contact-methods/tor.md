---
layout: page
title: Tor
author: mfc
language: pt
summary: Métodos de contato
date: 2018-09
permalink: /pt/contact-methods/tor.md
parent: /pt/
published: true
---

O Tor Browser é um navegador web focado em privacidade que permite interagir com sites de maneira anônima sem compartilhar sua localização (através do endereço IP) quando você acessa algum site.
[Panorama do Tor](https://www.torproject.org/about/overview.html.en)(em inglês), [outras informações sobre o TOR](https://www.torproject.org/pt-BR/). 
