---
layout: page
title: "Meu site caiu, o que aconteceu?"
author: Rarenet
language: pt
summary: "Ter seus site derrubados ou depredados é uma ameaça encarada por várias ONGs, mídias e produtoras de conteúdo independentes que, para além dos prejuízos, gera silenciamento."
date: 2023-04
permalink: /pt/topics/website-not-working/
parent: /pt/
---

# Meu site caiu, o que aconteceu?

Ter seus site derrubados ou depredados é uma ameaça encarada por várias ONGs, mídias e produtoras de conteúdo independentes que, para além dos prejuízos, gera silenciamento. É uma situação frustrante e pode ter diversas causas como baixa manutenção do site, hospedagem pouco confiável, [script-kiddies](https://pt.wikipedia.org/wiki/Script_kiddie), um ataque de 'negação de serviço' (DOS) ou uma invasão no site. Esta seção do Kit de Primeiros Socorros Digitais irá trilhar os caminhos básicos para diagnosticar problemas potenciais a partir do material [My Website is Down](https://github.com/OpenInternet/MyWebsiteIsDown/blob/master/MyWebsiteIsDown.md).

É importante saber que há muitas razões as quais seu site pode estar fora do ar. Pode variar de problemas técnicos na empresa de hospedagem a falta de atualizações no sistema de gestão de conteúdo (CMS) como o Joomla ou o WordPress. Encontrar o problema e a possível solução para o problema do seu site pode ser intrincado. É uma boa prática **contatar as pessoas responsáveis por hospedar e manter seu site*** após diagnosticar estes problemas comuns que iremos avaliar a seguir. Se nenhuma das opções estiver disponível, [busque uma organização de confiança](#website-down_end).

## Consider

Para começar, considere algumas perguntas:

- Quem desenvolveu o seu site? Estas pessoas estão disponíveis para ajudar?
- Seu site foi desenvolvido com WordPress, ou alguma outra plataforma de CMS conhecida?
- Qual é o seu provedor de hospedagem? Se você não sabe, pode usar um serviço [WHOIS](https://registro.br/tecnologia/ferramentas/whois/) para te ajudar.

## Workflow

### error-message

Você está vendo mensagens de erro?

 - [Sim, estou vendo mensagens de erro](#error-message-yes)
 - [Não](#hosting-message)


### hosting-message

Você está vendo uma mensagem do seu provedor de hospedagem?

- [Sim, estou vendo uma mensagem do meu provedor de hospedagem](#hosting-message-yes)
- [Não](#site-not-loading)


### site-not-loading

Seu não carrega de jeito nenhum, ou consegue carregar mesmo que lentamente?

- [Ele não carrega de jeito nenhum](#site-not-loading-yes)
- [Ele consegue carregar](#hosting-working)


### hosting-working

O site do provedor de hospedagem está acessível, mesmo que o seu não esteja?

- [Sim, o site do provedor de hospedagem está acessível](#hosting-working-yes)
- [Não](#similar-content-censored)


### similar-content-censored

Você consegue visitar outros sites com conteúdo similar ao seu?

- [Não consigo visitar outros sites com conteúdo similar, além do meu.](#similar-content-censored-yes)
- [Outros sites funcionam, só o meu que não.](#loading-intermittently)


### loading-intermittently

Seu site está carregando devagar, às vezes funciona e outras não?

- [Sim, está assim mesmo](#loading-intermittently-yes)
- [Não, ele está funcionando, mas foi invadido e modificado](#website-defaced)


### website-defaced

Seu site está carregando, mas o conteúdo e aparência não estão como devia ser?

- [Sim, o site não está com o conteúdo ou aparência que deveria](#defaced-attack-yes)
- [Não](#website-down_end)


### error-message-yes

> Você pode estar passando por uma ***falha de software*** -- reflita sobre quaisquer mudanças recentes que você ou sua equipe tenham feito, e contate a administração do site, enviando um print da mensagem de erro, o link da página com problemas, e quaisquer outros detalhes de erro que ajudem a identificar a causa do problema. Vale também fazer uma busca pela mensagem de erro para saber se existem soluções simples de ser aplicadas (atualizações de software, configurações locais etc)

Esta informação ajudou?

- [Sim](#resolved_end)
- [Não](#website-down_end)


### hosting-message-yes

>  Você pode ter tido sua página derrubada por uma ação judicial, [copyright](https://www.eff.org/issues/bloggers/legal/liability/IP), pagamento em atraso, ou outras razões legais. Contate a empresa de hospedagem para mais informações sobre a suspensão de sua hospedagem.

Esta informação ajudou?

- [Sim](#resolved_end)
- [Não, preciso de apoio jurídico](#legal_end)
- [Não, preciso de apoio técnico](#website-down_end)


### site-not-loading-yes

> O que você pode estar experimentando é um ***problema de hospedagem***. Você consegue visitar o site da empresa de hospedagem? Note que **não** estamos falando da página de administração do seu site, e sim da página da companhia ou organização que atua na hospedagem do site.
>
> Busque por uma página de "status" (como status.dreamhost.com), e tente também olhar no twitter.com se outros usuários estão comentando sobre quedas no perfil da empresa de hosting. É muito comum elas terem boa taxa de resposta no Twitter, e uma busca simples pelo nome da companhia e "down" ou "fora do ar" deve revelar se outros usuários estão passando pelo mesmo problema.

Esta informação ajudou?

- [Sim](#resolved_end)
- [Não, o site do provedor de hospedagem não está fora do ar](#hosting-working-yes)
- [Não, preciso de apoio técnico](#website-down_end)


### hosting-working-yes

> Verifique se o site está funcionando com o serviço [Down for Everyone or Just Me](https://downforeveryoneorjustme.com/) - seu site pode estar disponível online mas indisponível na sua localidade.
>
> Se o seu site está funcionando mas você não consegue vê-lo, provavelmente esteja sofrendo um ***problema na sua rede****: sua conexão de internet pode estar bloqueando o acesso ou indisponível no momento.

Necessita de mais alguma ajuda?

- [Não](#resolved_end)
- [Sim, preciso de ajuda para restaurar minha conexão](#website-down_end)
- [Sim, não é um problema de conexão e meu site está indisponível para mais pessoas](#similar-content-censored)


### similar-content-censored-yes

> Tente visitar sites com conteúdo similar ao seu. Além disso, tente usar o [Tor](https://www.torproject.org/projects/gettor.html) ou o [Psiphon](https://psiphon.ca/products.php) como formas de acessar seu site.
>
> Se você consegue acessar o site usando Tor ou Psiphon, pode estar lidando com ***uma situação de censura*** - você ainda está online para algumas partes do mundo, mas está sofrendo censura em seu próprio país.

Você acredita ter a ver com censura neste caso?

- [Sim, gostaria de denunciar publicamente e preciso de apoio para minha campanha de advocacy](#advocacy_end)
- [Sim, mas gostaria apenas de uma solução para colocar meu site no ar novamente sem exposição pública](#website-down_end)
- [Não](#resolved_end)


### loading-intermittently-yes

> Seu site pode estar sobrecarregado com o número e velocidade de solicitações por conexão - é um ***problema de desempenho***.
>
> Pode ser um sinal "positivo" que seu site se tornou mais popular, e por isso requer melhorias para estar responsivo para mais pessoas - verifique as ferramentas analíticas do seu site para identificar padrões de crescimento a longo prazo.  Contate a administração do seu site ou seu provedor de hospedagem para orientações. Muitas das plataformas de blogging e Sistemas de Gerenciamento de Conteúdo (ou CMS - como Joomla, Wordpress, Drupal etc) possuem plugins para ajudar você a manter um cache local do site e integrar com Redes de Fornecimento de Conteúdo [CDN](https://pt.wikipedia.org/wiki/Rede_de_fornecimento_de_conte%C3%BAdo), que podem aumentar drasticamente o desempenho e a resiliência do seu site. Muitas das soluções abaixo também podem auxiliar em problemas de desempenho.
>
> Se você está sofrendo **problemas de desempenho severos**, seu site pode estar sendo vítima de um [**ataque de negação de serviço distribuído**](https://pt.wikipedia.org/wiki/Ataque_de_nega%C3%A7%C3%A3o_de_servi%C3%A7o) (ou DDoS). Siga os passos abaixo para mitigar tal ataque:
>
> - Passo 1: Contate uma pessoa de confiança que possa ajudar seu site (a pessoa que administra ou desenvolve seu site, pessoas da sua organização ou seu serviço de hospedagem).
>
> - Passo 2: Trabalhe em conjunto com a empresa de quem você contratou o nome de domínio do seu site e mude o "Time to Live" (TTL) para 1 hora (você pode achar instruções sobre como fazer isso no site da maioria dos provedores de domínio, como [Network Solutions](http://www.networksolutions.com/support/how-to-manage-advanced-dns-records/) ou [GoDaddy](https://br.godaddy.com/help/gerenciar-os-registros-dns-680)). Isto pode ajudar a redirecionar seu site mais rápido uma vez que estiver sob ataque (o padrão geralmente são 72 horas, ou 3 dias). A configuração é geralmente encontrada na guia de propriedades "avançadas" (advanced) no painel do seu provedor de domínio, algumas vezes parte do SRV ou Service Records.
>
> - Passo 3: Mova seu site para um serviço de mitigação de DDoS. Para começar:
>
>     - [Deflect.ca](https://deflect.ca/)
>     - [Google's Project Shield](https://projectshield.withgoogle.com/en/)
>     - [CloudFlare's Project Galileo](https://www.cloudflare.com/galileo)
>
> Para uma lista completa de organizações confiáveis que podem auxiliar na mitigação de um ataque DDoS, vá para [a sessão final do fluxo que trata ataques DDoS](../ddos_end).
>
> - Passo 4: Uma vez que tenha retomado o controle do site, reveja suas necessidades e decida entre migrar para uma hospedagem focada em segurança ou manter apenas o serviço de mitigação de DDoS.

Para uma lista completa de organizações confiáveis que podem prover hospedagem segura, vá para [a sessão final do fluxo que trata problemas com hospedagem de sites](#web_hosting_end).

### defaced-attack-yes

> Depredação de sites (deface) é uma prática onde quem ataca modifica o conteúdo da página ou aparência visual com seu próprio conteúdo. Normalmente estes ataques são conduzidos visualmente explorando vulnerabilidades em plataformas CMS sem atualizações de segurança constante ou usando contas de hospedagem roubadas.
>
> Passo 1: Verifique se foi mesmo uma invasão maliciosa. Uma prática incômoda mas que infelizmente age na legalidade é comprar domínios que não foram renovados para "tomar pra si" o tráfego que eles geram e monetizar anúncios, além de valorizar preço de revenda do domínio (parking). É sempre importante manter o pagamento do seu domínio em ordem.
> - Passo 2: Se o seu site foi depredado, primeiro retome o acesso ao seu site e reset sua senha, se precisar de ajuda consulte a seção sobre Sequestro de Contas.
> - Passo 3: Faça um backup do site depredado para que possa ser usado para investigações futuras.
> - Passo 4: Tire seu site do ar temporariamente - use uma página de manutenção, ou uma página de boas vindas institucional simplficada.
> - Passo 5: Determine como seu site foi invadido. Sua empresa de hospedagem deve ser capaz de ajudar. Os motivos mais comuns são áreas mais antigas do seu site com trechos de código customizados obsoletos, plataformas desatualizadas, ou códigos com falhas de segurança estruturais.
> - Passo 6: Restaure o backup do seu site. Se nem você e nem a empresa de hospedagem possuem backups, você talvez precise refazer o site do zero! Note também que, se você possui backups apenas no serviço de hospedagem, quem atacou pode ter a capacidade de apagar todos eles caso tome controle dos seus acessos!

Estas recomendações ajudaram você?

- [Sim](#resolved_end)
- [Não](#website-down_end)


### website-down_end

> Se você ainda precisa de ajuda após as questões respondidas, você pode contatar uma organização de confiança e solicitar apoio.
>
> Antes de entrar em contato, reflita sobre as seguintes questões:
>
> - Como a empresa/organização que fornece hospedagem se estrutura e se mantém? Que tipos de auditoria ou relatorias são feitas por ela, se alguma?
> - Em quais países ela possui representação jurídica e quais as exigências necessárias para que esteja de acordo com as implicações legais destas jurisdições?
> - Quais registros são criados pelo seu site, e por quanto tempo são armazenados?
> - Existem restrições com relação ao tipo de conteúdo que o serviço irá hospedar/redirecionar, e poderiam causar impactos em seu site?
> - Existem restrições nos países em que os servidores estarão provisionados?
> - Ela aceita formas de pagamento que você pode usar? Você pode pagar por este serviço?
> - Comunicações seguras - é necessário que haja formas seguras de autenticar-se e comunicar-se privadamente com o serviço de hospedagem
> - Existe a possibilidade de autenticação de dois fatores, para aumentar a segurança do acesso ao painel de administração? Esta e outras políticas de segurança de acesso podem ajudar a reduzir a ameaça a outras formas de ataque contra o seu site.
> - Qual o tipo de suporte contínuo que você terá acesso? Você terá custos adicionais pelo suporte, ou receberá suporte suficiente caso esteja usando uma hospedagem "gratuita" (free tier)?
> - Você terá a habilidade de fazer um "test-drive" do seu site antes de colocar oficialmente no ar através de um site de homologação?

Segue uma lista de organizações que pode ajudar com sua situação:

:[](organisations?services=web_protection)

### legal_end

> Se o seu site foi derrubado por razões legais e você precisa de apoio jurídico, por gentileza entre em contato com uma das organizações que poderá te auxiliar:

:[](organisations?services=legal)

### advocacy_end

> Se você gostaria de apoio para lançar uma campanha contra censura, por gentileza entre em contato com organizações que podem auxiliar nos esforços de advocacy:

:[](organisations?services=advocacy)

### ddos_end

> Se você necessita ajuda para mitigar um ataque DDoS contra seu site, por gentileza contate organizações especializadas em mitigação DDoS:

:[](organisations?services=ddos)


### web-hosting_end

> Se você busca por uma organização de confiança para fornecer hospedagem segura a seu site, por gentileza consulte a lista a seguir:
>
> Antes de entrar em contato, reflita sobre as seguintes questões:
>
> - Ela oferece suporte integral para mover seu site atual para seus serviços?
> - Os serviços oferecidos são equivalentes ou melhores que os do seu serviço de hospedagem atual, ou possuem as ferramentas que você precisa? Coisas que você deve verificar são:
>     - Painel de gerenciamento simplificado, como o cPanel
>     - Contas de Email (limite de usuários, tamanho por usuário, acesso via SMTP e IMAP - para clientes de email)
>     - Bancos de dados (quantos, quais tipos, como acessar)
>     - Acesso remoto via SFTP/SSH
>     - Suporte à linguagem de programação (PHP, Perl, Ruby, cgi-bin...) ou CMS (Drupal, Joomla, Wordpress…) usada no seu site

Segue uma lista de organizações que podem auxiliar você com hospedagem de sites:

:[](organisations?services=web_hosting)


### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Backups** - Além dos serviços e sugestões abaixo, é sempre boa ideia se certificar de ter backups (armazenados em outro lugar além do seu serviço de hospedagem!). Muitos serviços também incluem a opção de ter backups em servidores externos, mas é sempre mais garantido ter também cópias offlines, seguindo também as recomendações de backups da seção Dados Perdidos.

- **Mantenha seu software atualizado** - Se estiver usando um sistema de gestão de conteúdo (CMS) como WordPress ou Drupal, certifique-se de manter a plataforma do site sempre atualizada, especialmente quando houver atualizações de segurança.

- **Monitoramento** - Há muitos serviços que podem verificar continuamente seus sites e enviar mensagens por texto ou email caso fiquem fora do ar. [Este artigo](https://tudosobrehospedagemdesites.com.br/como-monitorar-uptime-hospedagem/) lista opções gratuitas e como configurá-las, além de falar sobre outras opções disponíveis para ambientes mais complexos. Esteja ciente que o e-mail e o número que você usar nestes serviços ficará associado ao site e pode ficar visível a pessoas buscando informações sobre quem tem relação com ele.

#### Resources

- [DDoS: o que é, como funciona e como se proteger de ataques maliciosos na internet](https://www.hostinger.com.br/tutoriais/o-que-e-ddos-e-como-se-proteger-de-ataques)
+- [Como evitar ataques DDoS em meu servidor?](https://cliente.estadovirtual.com.br/knowledgebase/69/Como-evitar-ataques-DDoS-em-meu-servidor.html)
- [EFF: Keeping your site alive](https://www.eff.org/keeping-your-site-alive)

