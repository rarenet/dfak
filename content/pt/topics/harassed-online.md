﻿---
layout: page
title: "Você está sendo alvo de perseguição online??"
author: Flo Pagano, ***Natalia Krapiva***
language: pt
summary: "Você está sendo alvo de perseguição online?"
date: 2023-04
permalink: /pt/topics/harassed-online/
parent: pt
---

# Você está sendo alvo de perseguição online?

A Internet, especialmente as plataformas de mídia social, tem se tornado um espaço crítico para membros da sociedade civil e organizações, especialmente mulheres, pessoas LGBTQIAP+, pessoas negras, e outras maiorias silenciadas, para expressarem-se e fazerem suas vozes ouvidas. Mas ao mesmo tempo, também se tornam espaços onde estes grupos são facilmente alvos por expressarem suas perspectivas. Violência e abuso impedem estas e muitas outras pessoas desprovidas de privilégios, do direito à liberdade de expressão igualitária e sem medo de retaliações.

Violência e assédio online possuem diferentes formas, e agentes maliciosos comumente confiam na impunidade de seus atos devido à ausência de leis que protejam vítimes de perseguição em diversos países, mas especialmente pelo fato de tais ataques possuírem naturezas específicas e requerem estratégias de proteção criativas e personalizadas para serem evitados.

Por essa razão, é importante identificar o tipo dos ataque que você está sofrendo para tomar as medidas apropriadas de proteção.

Esta seção do Kit de Primeiros Socorros Digitais irá caminhar com você através dos passos básicos para planejar como se proteger contra ataques que estiver sofrendo.

Se você for alvo de perseguição online neste momento, sugerimos que você leia as recomendações na seção [Autocuidado e Cuidado Coletivo](/../pt/self-care) e a seção [Documentando Ataques Digitais](/../pt/documentation), então siga o questionário para identificar a natureza do problema e tentar encontrar possíveis soluções.


## Workflow

### physical-wellbeing

Você teme por sua integridade física e bem estar?

- [Sim](#physical-risk_end)
- [Não](#no-physical-risk)

### location

Parece que os agentes maliciosos sabem sua localização física?

- [Sim](#location_tips)
- [Não](#device)

### location_tips

> Verifique as suas publicações recentes nas redes sociais: elas incluem a sua localização exata? Se incluem, desative o acesso ao GPS para aplicações de redes sociais e outros serviços no seu celular, para quando publicar novamente sua localização não aparecer.
>
> Verifique as fotos que publicou online: elas incluem detalhes de locais que são claramente reconhecíveis e podem mostrar onde você se está? Para se proteger de potenciais perseguidores, é melhor não mostrar sua localização exata quando publicar fotografias ou vídeos.
>
> Como medida de precaução, é também uma boa prática manter o GPS sempre desligado - exceto por breves instantes, quando é realmente necessário encontrar sua posição em um mapa.

O pessoa que está te agredindo/perseguindo pode ter te seguido através das informações que você publicou online? Ou acha que descobriu sua localização física por outros meios?

 - [Não, acho que resolvi meus problemas](#resolved_end)
 - [Não. Ainda acho que o agressor sabe minha localização](#device)
 - [Não, mas tenho outras questões](#account)

### device

Você acha que a pessoa que está te agredindo/perseguindo acessou ou está acessando seu dispositivo?

 - [Sim](#device-compromised)
 - [Não](#account)


### device-compromised

> Troque sua senha de acesso do dispositivo: prefira usar uma frase com palavras aleatórias, que não digam respeito a algo pessoal, usando espaços e  caracteres especiais fáceis de lembrar. Mais dicas [neste link](https://ssd.eff.org/pt-br/module/criando-senhas-fortes).
>
> - [Mac OS](https://support.apple.com/pt-br/HT202860)
> - [Windows](https://support.microsoft.com/pt-br/help/14087/windows-7-change-your-windows-password)
> - [iOS - Apple ID](https://support.apple.com/pt-br/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=pt-BR&sjid=8286974989264976491-SA)

Ainda está com a sensação de que a pessoa que está te agredindo/perseguindo pode estar controlando seu dispositivo?

 - [Não, acho que já resolvi meus problemas](#resolved_end)
 - [Não, mas tenho outros problemas](#account)
 - [Não](#info_stalkerware)

### info_stalkerware

> [Stalkerware](https://tecnoblog.net/responde/o-que-e-stalkerware/) é qualquer software utilizado para monitorar a atividade ou a localização de uma pessoa com o objetivo de a perseguir ou controlar.
>
> [Se acha que alguém pode estar de espionando espiar através de uma aplicação que instalou no seu dispositivo, este tóopico do Kit de Primeiros Socorros Digitais vai te ajudar a decidir se o seu dispositivo está infetado com malware e como tomar medidas para limpá-lo.](../../pt/device-acting-suspiciously).

Precisa de mais ajuda para compreender o ataque que está sofrendo?

 - [Não, acho que já resolvi os meus problemas](#resolved_end)
 - [Sim, ainda tenho alguns problemas que gostaria de resolver](#account)

### account

> Quer alguém tenha tido acesso ao seu dispositivo ou não, é possível que tenha acessado suas contas online hackeado elas ou porque sabia ou decifrou sua senha.
>
> Se alguém tiver acesso a uma ou mais contas suas, pode ler suas mensagens privadas, identificar seus contatos, publicar mensagens, fotografias ou vídeos privados ou começar a se passar por você.
>
> Revise as atividade das suas contas online e sua caixa de mensagens (incluindo as pastas "Enviados" e "Lixo") para detectar possíveis atividades suspeitas.

Reparou o desaparecimento de publicações ou mensagens, ou outras atividades, o que te dá razões para pensar que sua conta pode ter sido comprometida?

 - [Sim](#account-compromised)
 - [Não](#private-contact)


### change passwords

> Tente alterar a senha de cada uma de suas contas online para uma mais forte e única.
>
> Saiba mais sobre como criar e administrar senhas mais seguras no [Guia de Autodefesa contra Vigilância](https://ssd.eff.org/pt-br/module/criando-senhas-fortes).
>
> Também é uma boa prática adicionar uma segunda camada de proteção às suas contas, ativando a autenticação de dois factores (2FA) sempre que possível.
>
> Saiba mais sobre como ativar a autenticação de dois fatores no [Guia de Autodefesa contra Vigilância](https://ssd.eff.org/pt-br/module/como-habilitar-autenticacao-de-dois-fatores).

Ainda tem a sensação de que alguém pode ter acesso a sua conta?

 - [Sim](#hacked-account)
 - [Não](#private-contact)


### hacked-account

> Se você não está conseguindo acessar sua conta, existe a chance dela ter sido hackeada (invadida) e que quem está invadindo tenha alterado a senha.

Se tem a sensação de que sua conta foi hackeada, acesse o tópico do Kit de Primeiro Socorros Digital que pode te ajudar com problemas de acesso à conta.

 - [Me leve para o tópico "Não consigo acessar minhas contas"](../../pt/account-access-issues)
 - [Acho que resolvi meus problemas](#resolved_end)
 - [Minha conta está ok, mas tenho outros problemas](#private-contact)


### private-contact

Você está recebendo ligações ou mensagens indesejadas nos aplicativos de conversa?

 - [Sim](#change_contact)
 - [Não](#threats)

### change_contact

> Se estiver recebendo chamadas indesejadas, mensagens SMS ou mensagens em aplicativos que estejam associados ao seu número de telefone, e-mail ou outras informações privadas de contato, pode tentar alterar o seu número, cartão SIN, e-mail ou outras informações associadas à conta.
>
> Você deve também considerar a possibilidade de denunciar e bloquear as mensagens e a conta associada à plataforma.

Conseguiu fazer parar as chamadas e mensagens indesejadas?

 - [Sim](#legal)
 - [Sim, mas tenho outros problemas](#threats)
 - [Não, Preciso de ajuda](#harassment_end)

### threats

Está sendo alvo de chantagem ou recebendo ameaças por e-mail ou mensagens na sua conta de redes sociais?

 - [Sim](#threats_tips)
 - [Não](#impersonation)

### threats_tips

> Se receber mensagens de ameaças, incluindo ameaças de violência física ou sexual, ou chantagem, deve documentar o máximo possível o que aconteceu. Grave as ligações e capturas de tela, denuncie a pessoa à plataforma ou ao prestador de serviços e bloqueie a pessoa que está te agredindo.

Conseguiu fazer parar as ameaças?

 - [Sim](#legal)
 - [Sim, mas tenho outros problema](#impersonation)
 - [Não, Preciso de ajuda](#legal-warning)

### impersonation

> Outra forma de assédio que você pode estar enfrentando é alguém se passar por você.
>
> Por exemplo, alguém pode ter criado uma conta usando seu nome e está enviando mensagens privadas ou atacando publicamente outras pessoas em plataformas de redes sociais, disseminando desinformação, discurso de ódio ou qualquer outra forma de comportamento prejudicial. Isso pode colocar você, sua organização ou pessoas próximas em risco de danos à reputação e segurança.
>
> Se suspeita que alguém está se passando por você ou por sua organização, pode acessar o tópico do Kit de Primeiros Socorros Digitais, que vai te orientar através das várias formas de falsificação de identidade para identificar a melhor estratégia possível para enfrentar o problema.

O que gostaria de fazer?

 - [Outra pessoa está se passando por mim online e gostaria de encontrar uma solução](../../pt/impersonated)
 - [Estou enfrentando um tipo diferente de assédio](#defamation)

### defamation

Alguém está tentando prejudicar sua reputação espalhando informações falsas?

 - [Sim](#defamation-yes)
 - [Estou enfrentando um tipo diferente de assédio](#defamation)

### defamation-yes

> A difamação é geralmente definida como fazer uma declaração que prejudica a reputação de alguém. A difamação pode ser falada (calúnia) ou escrita (injúria). As leis dos países podem ser diferentes sobre o que constitui difamação legalmente. Por exemplo, alguns países têm leis que protegem muito a liberdade de expressão, permitindo que a mídia ou pessoas falem coisas sobre pessoas públicas que possam ser embaraçosas ou prejudiciais, desde que acreditem que essas informações sejam verdadeiras. Outros países podem te permitir processar mais facilmente outras pessoas por divulgarem informações a seu respeito que não lhe agradam.
>
> Se alguém estiver tentando prejudicar sua reputação ou da sua organização, acesse o tópico do Kit de Primeiros Socorros Digitais sobre difamação, que vai te orientar sobre várias estratégias para lidar com campanhas de difamação.

O que gostaria de fazer?

 - [Quero acessar o tópico e encontrar uma estratégia para combater uma campanha de difamação](../../pt/defamation)
 - [Estou enfrentando um tipo diferente de assédio](#doxing)

### doxing

Tem alguém publicando informação pessoal ou fotos suas sem seu consentimento?

- [Sim](#doxing-yes)
- [Não](#hate-speech)

### doxing-yes

> Se alguém publicou informações privadas sobre você ou está divulgando vídeos, fotos ou outras mídias, acesse o tópico do Kit de Primeiros Socorros Digitais que pode te ajudar a compreender o que está acontecendo e como responder a esse ataque.

O que gostaria de fazer??

 - [Gostaria de acessar o tópico, entender o que está acontecendo e o que posso fazer](../../pt/doxing)
 - [Gostaria de receber suporte](#harassment_end)

### hate-speech

Este ataque é focado em raça, gênero ou religião?

- [Sim](#one-more-persons)
- [Não](#harassment_end)


### one-more-persons

O ataque foi feito por uma ou mais pessoas?

- [Uma pessoa](#one-person)
- [Mais de uma pessoa](#more-persons)

### one-person

> Se o discurso de ódio está vindo de uma única pessoa, a forma mais fácil e rápida de conter o ataque e impedir que a pessoa te agredindo continue enviando mensagens é denunciar e bloquear. Lembre-se que, se bloquear usuárie, não poderá acessar o conteúdo para documentar. Antes de bloquear, leia nossas [dicas em Documentando Ataques Digitais](/../pt/documentation).
>
> Sabendo ou não quem é que está te assediando, é sempre uma boa prática bloquear esta pessoa/perfil nas plataformas de redes sociais.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=pt-BR&sjid=8286974989264976491-SA)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/pt_BR/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/pt/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android)

Você bloqueou a pessoa que está te assediando?

- [Sim](#legal)
- [Não](#campaign)


### legal

> Se souber quem está te assediando, denuncie às autoridades do seu país, se considerar seguro e adequado no seu contexto. Cada país tem leis diferentes para proteger as pessoas do assédio online, e você deve recorrer à legislação ou pedir aconselhamento jurídico que te ajude a decidir o que fazer.
>
> Se não souber quem está te assediando, em alguns casos, você pode descobrir a identidade do agressor através de uma análise forense dos vestígios que ele possa ter deixado.
>
> Em todo o caso, se estiver pensando em entrar com uma ação judicial, será muito importante guardar provas das agressões. Por isso, recomendamos que siga as [recomendações da página do Kit Digital de Primeiros Socorros sobre o registo de informações sobre ataques digitais](/../pt/documentation).

O que gostaria de fazer?

 - [Gostaria de receber ajuda jurídica para processar o meu agressor](#legal_end)
 - [Acho que resolvi meus problemas](#resolved_end)


### campaign

> Se estiver sendo atacade por mais de uma pessoa, pode estar sendo alvo de uma campanha de discurso de ódio ou de assédio e terá de refletir sobre qual é a melhor estratégia para seu caso.
>
> Para conhecer todas as estratégias possíveis, leia a página [Take Back The Tech - estratégias para lidar com discurso de ódio](https://www.takebackthetech.net/be-safe/hate-speech-strategies) (em inglês e espanhol).

Identificou a melhor estratégia no seu caso?

 - [Sim](#resolved_end)
 - [Não, preciso de ajuda para resolver mais problemas](#harassment_end)
 - [Não, preciso de assistência legal](#legal_end)

### harassment_end

> Se ainda estiver sofrendo assédio e precisa de uma solução personalizada, entre em contato com as organizações abaixo que podem te ajudar.

:[](organisations?services=harassment)


### physical-risk_end

> Se estiver sob risco físico e precisa de ajuda imediata, entre em contato com as organizações abaixo que podem te ajudar.

:[](organisations?services=physical_security)


### legal_end

> Se precisar de apoio jurídico, contacte as organizações abaixo que podem te ajudar.
>
> Em todo o caso, se estiver pensando em entrar com uma ação judicial, será muito importante guardar provas das agressões. Por isso, recomendamos que siga as [recomendações](/../pt/documentation) do Kit Digital de Primeiros Socorros sobre como documentar informações sobre ataques digitais.

:[](organisations?services=legal)

### resolved_end

Esperamos que o Kit tenha sido útil para você. Será um prazer receber suas considerações [através deste email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- **Documente o assédio. É muito importante documentar os ataques ou qualquer outro incidente que possa testemunhar: faça capturas de tela, guarde as mensagens que recebe de quem está te assediando, etc. Se possível, crie um diário para sistematizar esta documentação, registando datas, horas, plataformas e URLs, ID de usuáries, capturas de tela, descrição do que aconteceu, etc. Os diários podem te ajudar a detectar possíveis padrões e indicações sobre possíveis agressores. Se se sentir sobrecarregade, pense em alguém de confiança que possa documentar os incidentes por você durante algum tempo. Você deve confiar profundamente na pessoa que fará essa documentação, já que precisa lhe entregar as credenciais das suas contas pessoais. Antes de compartilhar sua senha com essa pessoa, altere-a para algo diferente e partilhe-a através de um meio seguro, por exemplo, utilizando uma ferramenta com [criptografia de ponta a ponta](https://www.frontlinedefenders.org/pt/resource-publication/guide-secure-group-chat-and-conferencing-tools). Quando sentir que pode recuperar o controle da conta, lembre-se de [alterar novamente sua senha para algo único, seguro e que só você saiba](https://ssd.eff.org/pt-br/module/criando-senhas-fortes).

    - Sigas as [recomendações](/../pt/documentation) do Kit de Primeiros Socorros Digital sobre o registo de informações sobre ataques digitais para armazenar as informações relativas ao seu ataque da melhor maneira possível.

- **Ative a autenticação de dois fatores em todas as suas contas. Isso pode ser muito eficaz para impedir que alguém acesse suas contas sem sua autorização. Se puder escolher, não utilize a autenticação de dois fatores baseada em SMS, escolha uma opção diferente, de algum aplicativo ou chave de segurança (token).

    - Se não sabe qual solução é mais indicada para você acesse o [infográfico "Qual o melhor tipo de autenticação de dois fatores para mim?"](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) (em inglês) e [EFF's "Guide to Common Types of Two-Factor Authentication on the Web"](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web) (em inglês).
    - Você encontra instruções para a ativação da autenticação de dois fatores na maioria das plataforma em [12 dias de 2FA: como ativar a autenticação de dois fatores em minhas contas online](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts) (em inglês).

- **Mapeie sua presença online. O auto-doxing consiste em explorar informações públicas sobre si no ambiente online, evitando que agentes malicioses encontrem e utilizem estas informações para se passarem por você. Saiba mais sobre como pesquisar os seus vestígios online no [Guia da Linha de Ajuda da Access Now para evitar doxing](https://guides.accessnow.org/self-doxing.html) (em inglês).
- **Não aceite mensagens de remetentes desconhecidos. Algumas plataformas de mensagens, como o WhatsApp, o Signal ou o Facebook Messenger, te permitem pré-visualizar as mensagens antes de aceitar o remetente como confiável. O Apple iMessage também permite alterar as definições para filtrar mensagens de remetentes desconhecidos. Nunca aceite a mensagem ou o contato se parecerem suspeitos ou se não conhecer o remetente.

#### Resources

- [Access Now Helpline Community Documentation: Guia para prevenção de doxing](https://guides.accessnow.org/self-doxing.html) (em inglês)
- [Access Now Helpline Community Documentation: FAQ - Assédio Online Contra Membros da Sociedade Civil](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)
- [PEN America: Manual de Campo sobre Assédio Online](https://onlineharassmentfieldmanual.pen.org/) (em inglês)
- [Equality Labs: Guia anti-doxing para activistas que enfrentam ataques da direita rádical](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c) (em inglês)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/) (em inglês)
- [Rede Nacional para Acabar com a Violência Doméstica: Dicas de documentação para sobreviventes de abuso e perseguição tecnológico](https://www.techsafety.org/documentationtips) (inglês)
