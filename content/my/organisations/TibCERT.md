---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: "တနင်္လာမှသောကြာ ၊ GMT+5.30"
response_time: "ရုံးချိန်ပြင်ပဖြစ်ပါက ၂၄ ရီ ၊ ရုံးချိန်အတွင်းဖြစ်ပါက ၂ နာရီ"
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Tibetan Computer Emergency Readiness Team (TibCERT) သည် တိဘက်အသိုင်းအဝိုင်းတွင် အွန်လိုင်းခြိမ်းခြောက်မှုများကို လျော့နည်းစေရန်အတွက် အဖွဲ့များ ပူးပေါင်းဖွဲ့စည်းထားသည့် တရားဝင်အဖွဲ့ ဖွဲ့ရန် ကြိုးစားပါသည်။ TibCERT သည် တိဘက်ဒေသတွင်း ရွှေ့ပြောင်းလှုပ်ရှားမှုများ ဆိုင်ရာ စောင့်ကြည့်ခံရခြင်း နှင့် ဆင်ဆာဖြတ်တောက်မှုခံရခြင်းများတို့၏ ခြိမ်းခြောက်မှုများကို ခုခံတားစီးနိုင်ရန် နည်းပညာပိုင်း သုတေသန စွမ်းရည်ကို တိုးချဲ့ရန် လုပ်ဆောင်ပါသည်။ TibCERT ၏ ရည်ရွယ်ချက်ကတော့ တိဘက်အသိုင်းအဝိုင်း တစ်ခုလုံးအတွက် အွန်လိုင်းလွတ်လပ်ခွင့် နှင့် လုံခြုံမှု ပိုမိုရရှိစေရန် အတွက်ဖြစ်ပါသည်။

TibCERT ၏ မစ်ရှင်တွင် -
- ဒီဂျစ်တယ် လုံခြုံရေးကိစ္စများ နှင့် လိုအပ်ချက်များ ဆိုင်ရာ တိဘက် အသိုင်းအဝိုင်းတွင်း သက်ဆိုင်သူများအကြား ရေရှည်ပူးပေါင်းဆောင်ရွက်မှုပြုနိုင်ရန် အတွက် ပလက်ဖောင်းတစ်ခုကို ဖန်တီးပေးခြင်း နှင့် ထိန်းသိမ်းပေးခြင်း
- တိဘက်လူမျိုးများနှင့် ကမ္ဘာအနှံ့ရှိ သူခိုးဆော့ဝဲ နှင့် ဆိုင်ဘာလုံခြုံရေး သုတေသီများ အကြား အပြန်အလှန် အကျိုးရှိစွာ မျှဝေနိုင်ရန် အတွက် ဆက်သွယ်ချိတ်ဆက်မှုများ ပိုမိုနက်ရှိုင်းစေခြင်း နှင့် တိကျ၍ ပူးပေါင်းဆောင်ရွက်နိုင်သော လုပ်စဥ်များ တည်ဆောက်ခြင်း
- တိဘက်လူမျိုးများ အတွက် အွန်လိုင်းတိုက်ခိုက်မှုများကို ကာကွယ်လျော့ချနိုင်ရန် အတွက် အသိုင်းအဝိုင်းမှ တွေ့ကြုံရသော ခြိမ်းခြောက်မှုများဆိုင်ရာ အချက်အလက်များ နှင့် အကြံပေးချက်များကို ပုံမှန် ထုတ်ပြန်ခြင်း
- ပုံမှန်အသေးစိတ် အချက်အလက်များ၊ သုံးသပ်မှုများ နှင့် ဖြေရှင်းချက်များကို ပေးဆောင်၍ တိဘက်လူမျိုးများကို ဆင်ဆာဖြတ်တောက်ခံရခြင်း နှင့် စောင့်ကြည့်ခံရခြင်းတို့မှ သွယ်ဝိုက်နိုင်ရန် ပံ့ပိုးကူညီပေးခြင်း
