---
name: Civilsphere Project
website: https://www.civilsphereproject.org/
logo: Civilsphere.png
languages: English, Español
services: assessment, vulnerabilities_malware, forensic
beneficiaries: journalists, hrds, cso, activists, lawyers
hours: "երկուշաբթիից ուրբաթ, 09:00-17:00, ԿԵԺ"
response_time: "3 օր"
contact_methods: web_form, email, pgp, mail, telegram
web_form: www.civilsphereproject.org
email: civilsphere@aic.fel.cvut.cz
pgp_key: https://www.civilsphereproject.org/s/Civilsphere_Project_pub.asc
pgp_key_fingerprint: C1FD 513E D50F 00FE 6CBF C72F 52F7 76AD 9B72 6C6D
mail: "Stratosphere Lab (KN-E313), Karlovo náměstí 13, Praha 2, 121 35, Prague, Czech Republic"
telegram: "@civilsphereproject"
initial_intake: no
---

Civilsphere Project-ի առաքելությունն է լրագրողներին, ակտիվիստներին և իրավապաշտպաններին ծառայություններ ու գործիքներ տրամադրել՝ վաղաժամ հայտնաբերելու համար թվային սպառնալիքները, որոնք կարող են վտանգել նրանց կյանքն ու աշխատանքը։
