---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: "երկուշաբթիից ուրբաթ, 24/5, ՀԿԺ-4"
response_time: "6 ժամ"
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

Deflect-ը վեբկայքերի անվտանգության համար անվճար ծառայություն է, որը պաշտպանում է քաղաքացիական հասարակություններին ու իրավապաշտպան խմբերին թվային հարձակումներից։
