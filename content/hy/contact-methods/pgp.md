---
layout: page
title: PGP
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2020-11
permalink: /hy/contact-methods/pgp.md
parent: /hy/
published: true
---

PGP-ն (կամ Pretty Good Privacy-ն) և դրա բաց կոդով տարբերակը՝ GPG-ն (Gnu Privacy Guard-ը), թույլ են տալիս գաղտնագրել ձեր էլ.նամակների բովանդակությունը։ Սա արվում է էլ.փոստի մատակարարից և այլ կազմակերպություններից ձեր նամակագրությունը գաղտնի պահելու համար։ Այնուամենայնիվ, այն փաստը, որ ձեր էլ.հասցեից կազմակերպության հասցեին նամակ է ուղարկվել, կարող է տեսանելի լինել իշխանությունների և օրենսդիր մարմինների համար։ Հետևաբար, այս գործընթացի համար կարող եք այլընտրանքային էլ.հասցե ստեղծել, որը կապ չի ունենա ձեր ինքնության հետ։

Ռեսուրսներ. [Access Now Helpline Community Documentation՝ անվտանգ էլ.փոստի մասին](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation. Mailvelope-ով էլ.փոստի գաղտնագրում. ուղեցույց սկսնակների համար](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Գաղտնիություն պահող էլ.փոստի մատակարարներ](https://www.privacytools.io/providers/email/)
