---
layout: sidebar.pug
title: "Mendokumentasikan Serangan Digital"
author: Constanza Figueroa, Patricia Musomba, Candy Rodríguez, Gus Andrews, Alexandra Hache, Nina
language: id
summary: "Tip untuk mendokumentasikan berbagai jenis keadaan darurat digital."
date: 2023-05
permalink: /id/documentation/
parent: Home
sidebar: >
  <h3>Baca lebih lanjut tentang cara mendokumentasikan keadaan darurat digital:</h3>
  <ul>
    <li><a href="https://acoso.online/barbados/report-to-law-enforcement/#general-tips">Tip umum Acoso.online, melaporkan kasus dan cara menyimpan bukti (dalam bahasa Inggris)</a></li>
    <li><a href="https://www.techsafety.org/documentationtips">Jaringan Nasional untuk Akhiri Kekerasan Dalam Rumah Tangga: Tip Dokumentasi untuk Penyintas Penyalahgunaan dan Penguntitan Teknologi (dalam bahasa Inggris)</a></li>
    <li><a href="https://chayn.gitbook.io/how-to-build-a-domestic-abuse-case-without-a-lawye/english">Chayn: Bagaimana Menyiapkan Bukti untuk Kasus Kekerasan Dalam Rumah Tangga Tanpa Pengacara (dalam bahasa Inggris)</a></li>
      </ul>
---

#  Mendokumentasikan Serangan Digital

Mendokumentasikan Serangan Digital dapat menjadi berguna dalam beberapa cara. Merekam apa yang terjadi saat mengalami serangan dapat membantu Anda:

* memahami situasinya dengan lebih baik dan mengambil tindakan untuk melindungi diri sendiri.
* menyediakan bukti yang akan dibutuhkan oleh tim bantuan teknis atau hukum untuk membantu Anda.
* memahami pola serangan dengan lebih baik, dan mengidentifikasi ancaman tambahan.
* membantu Anda untuk lebih tenang secara pikiran dan memahami bagaimana serangan tersebut berdampak pada Anda secara emosional.

Apakah dokumentasi dilakukan untuk pemahaman Anda sendiri, atau untuk mencari bantuan hukum atau teknis, catatan serangan yang terstruktur dapat membantu Anda lebih memahami:

* Skala dan cakupan serangan: apakah serangan itu hanya satu kali atau pola yang berulang, yang ditargetkan pada Anda atau sekelompok orang yang lebih besar.
* Kebiasaan si penyerang: apakah mereka terisolasi atau bagian dari kelompok terorganisasi, menggunakan informasi yang tersedia di internet atau mengakses informasi pribadi Anda, taktik dan teknologi yang mungkin mereka gunakan, dll.
* Apakah respons yang Anda rencanakan untuk diterapkan akan mengurangi ancaman fisik atau daring terhadap Anda, atau memperburuknya.

Saat Anda mulai mengumpulkan informasi, pertimbangkan hal berikut:

* Apakah Anda mendokumentasikan terutama untuk ketenangan pikiran Anda sendiri dan/atau untuk memahami apa yang sedang terjadi?
* Apakah Anda ingin mencari bantuan teknis untuk menghentikan serangan? Harap pertimbangkan untuk mengunjungi [Bagian bantuan Pertolongan Pertama Darurat Digital](../support/) untuk melihat organisasi CiviCERT mana yang dapat membantu Anda.
* Apakah Anda juga ingin melanjutkan ke tindakan hukum?

Tergantung pada tujuan Anda, Anda mungkin perlu mendokumentasikan serangan tersebut dengan cara yang sedikit berbeda. Jika Anda memerlukan bukti untuk kasus hukum, Anda perlu mengumpulkan jenis bukti tertentu yang dapat diterima secara hukum di pengadilan, seperti nomor telepon dan cap waktu. Tim asisten teknis perlu melihat bukti tambahan, seperti alamat web (URL), nama pengguna, dan tangkapan layar. Lihat bagian di bawah ini untuk detail lebih lanjut tentang informasi yang harus Anda tangkap untuk setiap kemungkinan jalur ini.

## Lindungi kesejahteraan mental dan emosional Anda saat mendokumentasikan

Berurusan dengan serangan digital sangat melelahkan secara emosi, dan kesejahteraan fisik dan emosional Anda harus menjadi prioritas utama Anda, apa pun jenis dokumentasi yang ingin Anda kejar. Dokumentasi seharusnya tidak menambah beban stres Anda. Pertimbangkan hal berikut saat Anda mulai mendokumentasikan serangan:

* Ada banyak cara untuk mendokumentasikan dan merekam. Pilih salah satu yang Anda merasa nyaman dan mendukung tujuan Anda untuk dokumentasi.
* Mendokumentasikan serangan digital dapat menuntut lebih secara emosional dan memicu kenangan traumatis. Jika Anda merasa kewalahan, pertimbangkan untuk mendelegasikan dokumentasi kepada kolega, teman, orang tepercaya lainnya, atau grup pemantau yang dapat tetap memperbarui proses dokumentasi serangan tanpa mengalami trauma ulang.
* Jika serangan tersebut diarahkan pada kolektif, pertimbangkan untuk merotasi tanggung jawab dokumentasi di antara orang yang berbeda.
* Persetujuan adalah hal yang sangat penting. Dokumentasi harus dilanjutkan selama orang atau kelompok yang terlibat setuju.
* [Berikut adalah beberapa tip lainnya tentang cara membuat teman atau keluarga membantu Anda (dalam bahasa Inggris).](https://onlineharassmentfieldmanual.pen.org/guidelines-for-talking-to-friends-and-loved-ones/)

Mendokumentasikan serangan digital atau kekerasan berbasis gender dalam jaringan (daring) berarti mengumpulkan informasi tentang apa yang Anda atau kolektif Anda hadapi. Dokumentasi ini tidak hanya harus rasional dan teknis. Hal tersebut juga dapat membantu Anda memproses kekerasan dengan merekam perasaan Anda tentang setiap serangan dalam bentuk teks, gambar, audio, video, atau bahkan ekspresi artistik. Kami merekomendasikan melakukan ini secara luring dan mengambil langkah-langkah yang dijelaskan di bawah untuk melindungi privasi Anda saat memproses perasaan ini.

## Get ready to document an attack

Setelah memastikan kesejahteraan Anda sendiri, tetapi sebelum Anda mendokumentasikan setiap bukti, **membuat “peta” dari semua informasi yang relevan** tentang serangan tersebut dapat memandu Anda dalam mengidentifikasi semua bukti yang mungkin ingin Anda kumpulkan.

Bagian terpenting dari dokumentasi adalah **menyimpan catatan (atau “log”) yang teratur**. Untuk dokumentasi hukum atau teknis, log yang hanya berisi potongan data kunci adalah hal yang penting untuk membantu menunjukkan pola serangan dengan jelas. Saat mendokumentasikan untuk ketenangan pikiran Anda sendiri, dokumen teks biasa dengan struktur yang lebih sedikit mungkin cukup untuk Anda. Log yang lebih terstruktur juga dapat membantu Anda memikirkan pola serangan yang Anda hadapi. Log digital Anda bisa berupa dokumen teks atau *spreadsheet*. Anda juga bisa membuat jurnal di atas kertas. Pilihannya ada di tangan Anda.

**Semua interaksi digital meninggalkan jejak (metadata) yang menggambarkannya**: Waktu, durasi, alamat, akun pengirim dan penerima, dll. Jejak ini disimpan dalam log oleh peranti Anda sendiri, melalui perusahaan telepon dan media sosial, oleh penyedia internet, dan oleh orang lain. Menganalisis hal-hal tersebut dapat memberikan informasi penting kepada pakar hukum dan teknis tentang siapa yang berada di balik serangan. Misalnya, catatan nomor telepon yang digunakan dalam 20 panggilan dalam jangka waktu tertentu dapat mendukung laporan gangguan.

Menyimpan data yang terorganisir dan terstruktur dari data tersebut akan membantu Anda mendemonstrasikan **pola serangan** yang dapat digunakan untuk mendukung kasus hukum, atau membantu pakar keamanan digital memblokir serangan dan melindungi peranti Anda.

Informasi berikut akan berguna dalam banyak kasus, tetapi setiap kasus mungkin berbeda. Anda dapat menyalin dan menempelkan bidang di bawah ini ke bagian atas *spreadsheet* atau tabel dokumen untuk menyusun laporan Anda pada setiap serangan, tambahkan lebih banyak bidang sesuai kebutuhan.

| Tanggal | Waktu | Alamat surel atau nomor telepon yang digunakan si penyerang | Tautan yang berhubungan dengan serangan | Nama (termasuk nama akun) yang digunakan si penyerang | Jenis serangan digital, gangguan, misinformasi, dll. |   Teknik yang digunakan si penyerang |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |

Berikut adalah beberapa contoh templat log lain dari komunitas kami yang dapat Anda gunakan:

* [Templat log insiden dari Saluran Bantuan Keamanan Access Now ](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/incident_log_template.md)
* [Templat kekerasan berbasis gender dari Acoso.online](https://acoso.online/site2022/wp-content/uploads/2019/01/reactingNCP.pdf)

Selain berkas log, **simpan semua yang terkait dengan peristiwa atau insiden** dalam satu atau beberapa folder — termasuk dokumen log Anda dan bukti digital tambahan apa pun yang telah Anda ekspor, unduh, atau ambil dalam tangkapan layar. Bahkan bukti yang tidak memiliki bobot hukum dapat berguna untuk menunjukkan besarnya serangan dan membantu Anda merencanakan strategi respons.

**Simpan informasi yang Anda kumpulkan dengan cara yang aman**. Lebih aman untuk menyimpan cadangan data di peranti Anda sendiri (tidak hanya di daring atau "di *cloud*") Lindungi berkas tersebut dengan enkripsi, dan di folder tersembunyi atau partisi *drive* jika memungkinkan. Anda tidak ingin kehilangan catatan ini atau, lebih buruk lagi, membiarkannya terekspos.

Di media sosial, **hindari pemblokiran, pembisuan, atau pelaporan akun yang menyerang Anda** hingga Anda mengumpulkan dokumentasi yang diperlukan, karena hal ini dapat menghentikan Anda menyimpan bukti dari akun tersebut. Jika Anda sudah memblokir, membisukan, atau melaporkan akun di media sosial, jangan khawatir. Anda mungkin dapat membuka blokir atau membatalkan pembisuan akun yang tidak dapat Anda lihat lagi, atau dokumentasi mungkin masih dapat dilakukan dengan melihat akun tersebut dari akun teman atau kolega.

## Take Screenshots

Jangan meremehkan pentingnya mengambil tangkapan layar dari semua serangan (atau meminta seseorang yang Anda percaya melakukannya untuk Anda). Banyak pesan digital yang mudah untuk dihapus atau hilang jejaknya. Beberapa, seperti Tweet atau pesan WhatsApp, hanya dapat dipulihkan dengan bantuan platform, yang seringkali membutuhkan waktu lama untuk menanggapi laporan serangan, itu pun jika mereka meresponsnya.

Jika Anda tidak tahu cara mengambil tangkapan layar di peranti Anda, lakukan pencarian daring untuk “cara *screenshot*” dengan merek, model, dan sistem operasi peranti Anda (misalnya, “cara *screenshot* Android Samsung Galaxy S21”).

Jika Anda mengambil tangkapan layar pesan di *browser* web, penting untuk **menyertakan alamat (URL) halaman di gambar yang diambil**. URL ada di bilah alamat di bagian atas jendela *browser*; terkadang *browser* menyembunyikan dan menampilkannya saat Anda menggulir jendela *browser*. Alamat URL tersebut membantu memverifikasi di mana pelecehan terjadi dan membantu pakar teknis dan hukum menemukannya dengan lebih mudah. Di bawah ini Anda dapat melihat contoh tangkapan layar yang menunjukkan URL.

<img src="../../images/Screenshot_with_URL.png" alt="Here is a sample view of a screenshot showing the URL." width="80%" title="Berikut adalah contoh tampilan tangkapan layar yang menunjukkan URL.">

<a name="legal"></a>
## Documenting for a legal case

Pertimbangkan apakah Anda ingin mengambil tindakan hukum. Apakah itu akan memaparkan Anda pada risiko tambahan? Bisakah Anda membayar waktu dan upaya yang terlibat? Melanjutkan ke jalur hukum tidak selalu wajib atau perlu. Juga pertimbangkan apakah tindakan hukum tersebut akan dapat memulihkan Anda.

Jika Anda memutuskan untuk membawa kasus tersebut ke pengadilan, mintalah nasihat dari pengacara atau organisasi hukum yang Anda percayai. Nasihat hukum yang buruk dapat menambah beban stres dan merusak. Jangan mengambil tindakan hukum sendiri kecuali Anda benar-benar tahu apa yang Anda lakukan.

Di sebagian besar yurisdiksi, seorang pengacara perlu menunjukkan bahwa bukti tersebut relevan dengan kasusnya, bagaimana itu diperoleh dan diverifikasi, bahwa bukti tersebut dikumpulkan dengan cara yang sah, bahwa mengumpulkannya diperlukan untuk melanjutkan kasus, dan fakta lain yang membuat bukti tersebut dapat diterima oleh pengadilan. Setelah pengacara menunjukkan hal tersebut, bukti akan dievaluasi oleh hakim atau pengadilan.

Hal-hal yang perlu diperhatikan saat mendokumentasikan untuk kasus hukum:

* Sangat penting untuk menyimpan bukti dengan cepat, sejak awal serangan, karena konten dapat dengan cepat dihapus oleh pembuat posting atau situs media sosial, sehingga lebih sulit untuk menyimpan bukti untuk tindakan hukum di kemudian hari..
* Sekalipun serangan itu tampak kecil, menyimpan semua bukti itu penting. Hal tersebut mungkin bagian dari pola yang hanya akan terbukti menjadi kriminal jika Anda melihatnya dari awal sampai akhir.
* Anda mungkin ingin meminta platform atau penyedia internet untuk menyimpan informasi tentang serangan digital untuk Anda. Ini hanya mungkin untuk waktu yang singkat (beberapa minggu atau sebulan) setelah kejadian berlangsung. Lihat [Informasi Without My Consent tentang mengajukan permintaan penangguhan litigasi](https://withoutmyconsent.org/resources/something-can-be-done-guide/evidence-preservation/#consider-whether-to-include-a-litigation-hold-request) untuk informasi lebih lanjut tentang cara melakukannya.
* Cap waktu dan surel serta alamat web sangat penting agar bukti dapat diterima di pengadilan. Pengacara harus menunjukkan bahwa pesan berpindah dari satu peranti ke peranti lainnya pada hari tertentu dan pada waktu tertentu dengan menggunakan alat-alat tersebut.
* Pengacara juga perlu memverifikasi bahwa bukti tersebut belum dirusak, bahwa bukti tersebut berada di tangan yang aman sejak Anda menyimpannya, dan bahwa bukti tersebut asli. Tangkapan layar dan cetakan surel tidak dapat dianggap sebagai bukti yang cukup kuat dalam hal ini.

Untuk alasan hukum tersebut, sebaiknya Anda menyewa seorang ahli seperti notaris atau perusahaan sertifikasi digital yang dapat memberikan kesaksian di pengadilan jika diperlukan. Perusahaan sertifikasi digital seringkali lebih murah daripada sertifikat yang diaktakan. Notaris atau perusahaan sertifikasi mana pun yang Anda pekerjakan harus memiliki latar belakang teknis yang cukup untuk melakukan tugas seperti ini jika diperlukan:

* mengonfirmasi cap waktu
* memverifikasi keberadaan atau ketidakberadaan konten tertentu, seperti membandingkan tangkapan layar Anda dan bukti lain yang dikumpulkan dengan sumber aslinya, untuk memastikan bahwa bukti Anda tidak dirusak
* menawarkan bukti bahwa sertifikat digital cocok dengan URL
* memverifikasi identitas, seperti memeriksa apakah seseorang muncul dalam daftar profil di platform media sosial dan bahwa mereka memiliki profil dengan nama panggilan yang menyerang Anda
* mengonfirmasi kepemilikan nomor telepon dalam percakapan WhatsApp

Ketahuilah bahwa tidak semua bukti yang Anda kumpulkan akan diterima oleh pengadilan. Teliti prosedur hukum terkait ancaman digital di negara dan wilayah Anda untuk memutuskan apa yang akan Anda kumpulkan.

### Documenting human rights violations

Jika Anda perlu mendokumentasikan pelanggaran hak asasi manusia, dan mengunduh atau menyalin materi yang dihapus secara aktif oleh platform media sosial, Anda mungkin ingin menghubungi organisasi seperti [Mnemonic]( https://mnemonic.org/en/our-work) yang dapat membantu mempertahankan bahan-bahan tersebut.

## How to save evidence for legal and digital forensic reports

Selain menyimpan tangkapan layar, berikut beberapa instruksi lebih lanjut tentang cara menyimpan bukti secara lebih menyeluruh yang dapat membantu kasus Anda dan pekerjaan seseorang yang memberi Anda bantuan hukum atau teknis.

* **Log panggilan telepon.** Nomor panggilan masuk dan keluar disimpan dalam basis data di ponsel Anda.
    * Anda dapat mengambil tangkapan layar log.
    * Mungkin juga untuk menyimpan log tersebut dengan mencadangkan sistem ponsel Anda atau menggunakan perangkat lunak khusus untuk mengunduh log.
* **Pesan teks (SMS)**: Sebagian besar ponsel memungkinkan Anda mencadangkan pesan SMS ke cloud. Alternatif lain, Anda dapat mengambil tangkapan layar dari pesan tersebut. Tak satu pun dari metode ini bisa dianggap sebagai bukti hukum tanpa verifikasi tambahan, namun tetap penting untuk mendokumentasikan serangan.
    * Android: jika ponsel Anda tidak memiliki kemampuan untuk mencadangkan pesan SMS, Anda dapat menggunakan aplikasi seperti [SMS Backup & restore](https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore).
    * iOS: Aktifkan cadangan pesan di sini: iCloud di Pengaturan > [nama pengguna Anda] > Kelola penyimpanan > Cadangan. Ketuk nama peranti yang Anda gunakan, dan aktifkan pencadangan pesan.
* **Rekaman panggilan**. Ponsel yang lebih baru dilengkapi dengan perekam panggilan yang terpasang. Periksa seperti apa aturan hukum di negara atau yurisdiksi lokal Anda tentang merekam panggilan dengan atau tanpa persetujuan kedua belah pihak dalam panggilan; dalam beberapa kasus, hal ini mungkin ilegal. Jika sah untuk melakukannya, Anda dapat mengatur perekam panggilan untuk aktif saat Anda menjawab panggilan, tanpa memberi tahu penelepon bahwa mereka sedang direkam.
    * Android: di Pengaturan > Telepon > Pengaturan rekaman panggilan Anda dapat mengaktifkan perekaman otomatis untuk semua panggilan, panggilan dari nomor tak dikenal, atau dari nomor tertentu. Jika opsi ini tidak tersedia di perangkat Anda, Anda dapat mengunduh aplikasi seperti [Call Recorder](https://play.google.com/store/apps/details?id=com.lma.callrecorder).
    * iOs: Apple lebih membatasi terhadap perekaman panggilan dan telah memblokir opsi tersebut secara bawaan. Anda perlu memasang aplikasi seperti [RecMe](https://apps.apple.com/us/app/call-recorder-recme/id1455818490).
* **Surel**. Setiap surel memiliki “*header*” yang berisi informasi tentang siapa yang mengirim pesan dan bagaimana caranya, mirip seperti alamat dan cap pos pada surat kertas.
    * Untuk instruksi tentang cara melihat dan menyimpan *header* surel, Anda dapat menggunakan [panduan oleh Computer Incident Response Center Luxembourg (CIRCL) ini](https://www.circl.lu/pub/tr-07/).
    * Jika Anda memiliki lebih banyak waktu, Anda mungkin juga ingin mengatur surel Anda untuk mengunduh semua pesan ke klien desktop seperti Thunderbird dengan menggunakan [POP3](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3#w_changing-your-account). Instruksi tentang cara mengatur akun surel di Thunderbird dapat ditemukan di [dokumentasi resmi](https://support.mozilla.org/en-US/kb/manual-account-configuration).
* **Foto**. Semua gambar memiliki “label EXIF” - metadata yang dapat memberi tahu Anda di mana dan kapan foto diambil.
    * Anda mungkin dapat menyimpan gambar dari situs web atau pesan yang Anda terima dengan cara menyimpan label EXIF, yang akan hilang jika Anda baru saja mengambil tangkapan layar. Tekan dan tahan gambar di peranti seluler, atau klik kanan gambar di komputer (ctrl-klik di Mac, gunakan tombol menu di Windows)
* **Situs web**. Mengunduh atau mencadangkan halaman web secara lengkap dapat bermanfaat bagi mereka yang mencoba membantu Anda.
    * Jika halaman tempat terjadinya gangguan bersifat publik (dengan kata lain, Anda tidak perlu masuk untuk melihatnya), Anda dapat memasukkan alamat halaman tersebut ke web Internet Archive [Wayback Machine](https://web.archive.org/) untuk menyimpan halaman, catat tanggal saat Anda menyimpannya, dan kembali ke halaman tersimpan untuk tanggal tersebut nanti.
    * Tangkapan layar memberikan detail yang lebih sedikit, tetapi juga dapat menyimpan informasi penting.
* **WhatsApp**. Percakapan WhatsApp dapat diunduh dalam format teks atau dengan media terlampir; percakapan tersebut juga dicadangkan secara bawaan ke iCloud atau Google Drive.
    * Ikuti instruksi untuk mengekspor riwayat obrolan individu atau grup [di sini](https://faq.whatsapp.com/1180414079177245/?helpref=uf_share).
* **Telegram**. Untuk mengekspor percakapan di Telegram, Anda harus menggunakan aplikasi desktop. Anda dapat memilih format dan jangka waktu yang ingin diekspor, dan Telegram akan menghasilkannya dalam berkas HTML. Lihat instruksinya [di sini](https://telegram.org/blog/export-and-more).
* **Facebook Messenger**. Masuk ke akun Facebook Anda. Buka Pengaturan > Informasi Anda di Facebook > Unduh salinan informasi Anda. Akan muncul banyak pilihan: pilih "Pesan". Facebook membutuhkan waktu untuk memproses berkas tersebut, tetapi akan memberi tahu Anda saat berkas tersedia untuk diunduh.
* Jika Anda perlu menyimpan **videos** sebagai bukti, siapkan *drive* eksternal (diska keras, stik USB, atau kartu SD) dengan ruang penyimpanan yang cukup. Anda mungkin perlu menggunakan alat penangkap layar video. Aplikasi Kamera, XBox Game Bar, atau Snipping Tool dapat digunakan untuk hal tersebut di Windows, Quicktime di Mac, atau Anda dapat menggunakan plugin browser seperti [Video DownloadHelper](https://www.downloadhelper.net/) untuk menyimpan video.
* Dalam kasus serangan terorganisasi berskala besar — misalnya, semua posting dengan **tagar tertentu, atau komentar daring dalam jumlah besar** — Anda mungkin memerlukan bantuan dari tim keamanan, lab forensik digital, atau universitas untuk mengumpulkan dan memproses banyak data. Organisasi yang mungkin Anda hubungi untuk meminta bantuan termasuk [Citizen Lab](https://citizenlab.ca/about/), [K-Lab Fundacion Karisma](https://web.karisma.org.co/klab/) (bekerja dalam bahasa Spanyol), [Meedan](https://meedan.com/programs/digital-health-lab), Observatorium Internet Stanford dan Institut Internet Oxford.
* **Berkas yang dilampirkan ke pesan berbahaya** adalah bukti yang berharga. Dalam situasi apa pun Anda tidak boleh mengeklik atau membukanya. Penasihat teknis tepercaya harus dapat membantu Anda menyimpan lampiran ini dengan aman dan mengirimkannya ke orang yang dapat menganalisisnya.
    * Anda bisa menggunakan [Danger Zone](https://dangerzone.rocks/) untuk mengonversi PDF, dokumen Office, atau gambar yang berpotensi berbahaya menjadi PDF yang aman.
* Untuk ancaman yang lebih tertarget, seperti **perangkat pengintai** di peranti Anda atau **surel berbahaya** yang dikirimkan khusus kepada Anda, mengumpulkan bukti teknis yang lebih mendalam dapat membantu kasus Anda. Penting untuk mengetahui kapan harus mengumpulkan bukti sendiri dan kapan harus menyerahkannya kepada para ahli. Jika Anda tidak yakin bagaimana menindaklanjutinya, hubungi [penasihat tepercaya](../support).
    * **Jika peranti tersebut hidup, biarkan menyala. Jika mati, biarkan saja.** Anda berisiko kehilangan informasi penting ketika mematikan atau menghidupkan peranti tersebut. Untuk penyelidikan yang sangat sensitif dan legal, sebaiknya libatkan pakar forensik sebelum mematikan atau menghidupkannya kembali.
    * Ambil gambar peranti yang menurut Anda berisi perangkat lunak berbahaya. Dokumentasikan kondisi fisik serta tempat Anda menemukannya saat Anda mulai mencurigai peranti tersebut dirusak. Apakah ada penyok atau goresan? Apakah perantinya basah? Apakah ada alat terdekat yang bisa digunakan untuk mengutak-atiknya?
    * Simpan peranti dan data yang telah Anda salin di lokasi yang aman.
    * Metode yang tepat untuk mengekstraksi data bergantung pada perantinya. Mengekstrak data dari laptop memiliki cara berbeda dengan dari gawai. Setiap peranti membutuhkan alat dan pengetahuan khusus.
* Pengumpulan data untuk ancaman yang ditargetkan sering melibatkan penyalinan **berkas log sistem** yang dikumpulkan oleh ponsel atau komputer Anda secara otomatis. Pakar teknis yang membantu Anda dengan pengumpulan data mungkin akan meminta hal tersebut. Mereka akan membantu Anda menyimpan metadata dan lampiran dengan aman dan mengirimkannya ke orang yang dapat menganalisisnya.
    * Untuk menjaga metadata tersebut, isolasi peranti dari sistem penyimpanan lain, matikan Wi-Fi dan Bluetooth, cabut koneksi jaringan kabel, dan jangan pernah menyunting berkas log.
    * Jangan mencolokkan stik USB dan mencoba menyalin berkas log ke dalamnya.
    * Berkas tersebut bisa jadi menyertakan informasi tentang status berkas di peranti (seperti bagaimana cara berkas diakses) atau tentang perantinya (seperti apakah perintah matikan atau hapus telah dijalankan, atau apakah seseorang telah mencoba menyalin berkas ke peranti lain).
