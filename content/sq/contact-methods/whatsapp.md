---
layout: page
title: WhatsApp
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/whatsapp.md
parent: /en/
published: true
---

Përdorimi i WhatsApp-it do të sigurojë që biseda juaj me marrësin të jetë e mbrojtur në mënyrë që vetëm ju dhe marrësi të mund të lexoni komunikimet, megjithatë fakti që keni komunikuar me marrësin mund të jetë i aksesueshëm nga qeveritë ose agjencitë e zbatimit të ligjit.

Burimet: [Si të: Përdorni WhatsApp-in në Android](https://ssd.eff.org/en/module/how-use-whatsapp-android) [Si të: Përdorni WhatsApp-in në iOS](https://ssd. eff.org/en/module/how-use-whatsapp-ios).