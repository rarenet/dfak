---
layout: page
title: Tor
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/tor.md
parent: /en/
published: true
---

Shfletuesi Tor është një shfletues uebi i fokusuar në privatësi që ju mundëson të ndërveproni me faqet e internetit në mënyrë anonime duke mos ndarë vendndodhjen tuaj (nëpërmjet adresës suaj IP) kur hyni në faqen e internetit.

Burimet: [Përmbledhje e Tor](https://www.torproject.org/about/overview.html.en).
