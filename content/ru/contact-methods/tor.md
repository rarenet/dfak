---
layout: page
title: Tor
author: mfc
language: ru
summary: Contact methods
date: 2018-09
permalink: /ru/contact-methods/tor.md
parent: /ru/
published: true
---

Tor Browser ориентирован на обеспечение приватности. Он позволяет подключаться к веб-сайтам анонимно и не раскрывает ваше местонахождение по IP-адресу, когда вы заходите на сайт.

Что почитать: [О Tor Browser](https://www.torproject.org/ru/about/history/), [Инструменты свободы слова: пиши анонимно, используй Tor](https://te-st.org/2020/04/30/anonymity-and-tor/), [Как быть, если Tor недоступен?](https://te-st.org/2021/12/04/tor-blocked/), 