---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: วันจันทร์ – วันพฤหัสบดี, 9:00 – 17:00 น. โซนเวลายุโรปกลาง (CET)
response_time: 4 วัน
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership (DDP) ทำงานด้านการช่วยเหลือนักปกป้องสิทธิมนุษยชนที่ตกอยู่ภายใต้ภัยอันตรายทางดิจิทัล และเสริมสร้างความเข้มแข็งให้กับเครือข่ายตอบสนองเร่งด่วนในพื้นที่ DDP ประสานงานเพื่อขอความช่วยเหลือฉุกเฉินให้กับบุคคลและองค์กร อันรวมไปถึงนักปกป้องสิทธิมนุษยชน นักข่าว นักกิจกรรมเคลื่อนไหวในภาคประชาสังคม และนักเขียนบล็อก

DDP มีเงินทุน 5 ประเภทสำหรับการทำงานเพื่อแก้สถานการณ์ฉุกเฉินและเพื่อเสริมสร้างศักยภาพในระยะยาวให้กับองค์กร นอกจากนี้ องค์กรนี้ยังจัดทำโครงการให้ทุนสำหรับการรักษาความครบถ้วนสมบูรณ์ทางดิจิทัล (Digital Integrity Fellowship) ซึ่งเป็นการจัดอบรมเกี่ยวกับความปลอดภัยและความเป็นส่วนตัวทางดิจิทัลสำหรับองค์กรผู้เข้าร่วมโครงการโดยเฉพาะ และโครงการเครือข่ายด้านการตอบสนองเร่งด่วน
