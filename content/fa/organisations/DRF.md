---
name: Digital Rights Foundation
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: ۷ روز در هفته، ۹ صبح تا ۵ بعد از ظهر UTC+5 (PST)
response_time: ۵۶ ساعت
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

بنیاد حقوق دیجیتال (DRF) یک سازمان غیردولتی حمایتی مبتنی بر تحقیقات ثبت شده در پاکستان است. DRF که در سال 2012 تأسیس شد، بر فناوری اطلاعات و ارتباطات برای حمایت از حقوق بشر، فراگیر بودن، فرآیندهای دموکراتیک و حکومت دیجیتال تمرکز دارد. DRF روی موضوعات آزادی بیان آنلاین، حریم خصوصی، حفاظت از داده ها و خشونت آنلاین علیه زنان کار می کند.

